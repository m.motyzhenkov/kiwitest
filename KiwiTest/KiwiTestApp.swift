//
//  KiwiTestApp.swift
//  KiwiTest
//
//  Created by Михаил Мотыженков on 24.04.2022.
//

import SwiftUI

@main
struct KiwiTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
